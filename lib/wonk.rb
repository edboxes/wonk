require "wonk/version"

require 'ice_nine'
require 'active_support'
require 'active_support/core_ext'

require 'logger'

module Wonk
  mattr_accessor :logger
  @logger = Logger.new($stderr)

  mattr_accessor :aws_region
end

Dir["#{__dir__}/**/*.rb"] \
  .each { |f| require_relative f }

