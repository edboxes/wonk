module Wonk
  class Concretizer
    CAPTURE_REGEXP = /!<(?<directive>.*?)>/

    class DirectiveEvaluator
      attr_reader :env

      def initialize(env)
        @env = env
      end

      def evaluate(directive)
        instance_eval directive
      end
    end

    def concretize(text, env)
      raise "'text' must be a String." unless text.is_a?(String)
      raise "'env' must be a Hash." unless env.is_a?(Hash)

      env = env.deep_dup.deep_symbolize_keys

      working = []
      until(working[-2] == "" && working[-1] == "") do
        if (working.size == 0)
          working = text.partition(CAPTURE_REGEXP)
        else
          working[-1] = working[-1].partition(CAPTURE_REGEXP)
          working = working.flatten
        end
      end
      working.reject! { |t| t.empty? }

      evaluator = DirectiveEvaluator.new(env)

      working.map do |token|
        match = CAPTURE_REGEXP.match(token)
        if match.nil?
          token
        else
          evaluator.evaluate(match['directive'])
        end
      end
    end
  end
end