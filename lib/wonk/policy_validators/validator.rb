require 'ice_nine'

module Wonk
  module PolicyValidators
    class Validator
      def initialize(parameters)
        raise "#{self.class.name} can't be directly initialized."
      end

      def validator_name
        raise "#{self.class.name}#validator_name must be implemented."
      end

      def authenticate_from_submission(submission)
        if !submission.key?(:authentication_type)
          ValidatorResult.new(successful: false, environment: {})
        else
          if submission[:authentication_type] == validator_name
            begin
              do_authenticate(submission)
            rescue ValidatorError => err
              Wonk.logger.warn "Validator failed with reason: #{err.message}"
              ValidatorResult.new(successful: false, environment: {})
            rescue StandardError => err
              Wonk.logger.error "Unrecognized error rescued from validator #{self.class.name}: #{err}"
              ValidatorResult.new(successful: false, environment: {})
            end
          else
            ValidatorResult.new(successful: false, environment: {})
          end
        end
      end

      def do_authenticate(submission)
        raise "#{self.class.name}#do_authenticate(request) must be implemented."
      end
    end
  end
end
