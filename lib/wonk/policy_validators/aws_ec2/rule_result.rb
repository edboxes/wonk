module Wonk
  module PolicyValidators
    module AwsEC2
      class RuleResult
        attr_reader :captures

        def initialize(successful:, captures: nil)
          @successful = !!successful

          @captures = IceNine.deep_freeze((captures || []).deep_dup)
        end

        def success?
          @successful
        end
      end
    end
  end
end
