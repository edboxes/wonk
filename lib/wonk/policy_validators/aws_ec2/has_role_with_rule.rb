require 'wonk/policy_validators/aws_ec2/rule'

module Wonk
  module PolicyValidators
    module AwsEC2
      class HasRoleWithRule
        attr_reader :name

        def initialize(parameters)
          @name = Regexp.new(parameters[:name]) if parameters[:name]

          @iam_rsrc = Aws::IAM::Resource.new(region: Wonk.aws_region)
        end

        def try_match(instance, identity)
          if instance.iam_instance_profile.nil?
            RuleResult.new(successful: false)
          else
            instance_profile =
              @iam_rsrc.instance_profile(instance.iam_instance_profile.arn.split('/').last)

            roles = instance_profile.roles

            match_role = roles.map do |role|
              if @name.nil?
                [ role, true, {} ]
              else
                match = @name.match(role.name)

                if !match.nil?
                  [ role, true, Hash[match.names.zip(match.captures)] ]
                else
                  [ role, false, {} ]
                end
              end
            end.find { |rt| rt[1] == true }

            RuleResult.new(successful: !match_role.nil?,
                           captures: match_role[2] || {})
          end
        end
      end
    end
  end
end
