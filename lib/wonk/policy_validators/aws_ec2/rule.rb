require 'wonk/policy_validators/aws_ec2/rule_result'

module Wonk
  module PolicyValidators
    module AwsEC2
      class Rule
        def initialize(parameters)
          raise "#{self.class.name} can't be directly initialized."
        end

        def try_match(instance, identity)
          raise "#{self.class.name}#try_match(instance, identity) must be implemented."
        end
      end
    end
  end
end
