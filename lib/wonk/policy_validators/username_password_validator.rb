require 'bcrypt'

require 'wonk/policy_validators/validator'
require 'wonk/policy_validators/validator_result'

module Wonk
  module PolicyValidators
    class UsernamePasswordValidator < Validator
      def initialize(parameters)
        [:username, :password_hash].each do |n|
          raise "parameter '#{n}' required for #{self.class.name}" unless parameters.key?(n)
        end

        @username = parameters[:username]
        @password_hash = BCrypt::Password.new(parameters[:password_hash])
      end

      def validator_name
        'username-password'
      end

      def do_authenticate(submission)
        [ :username, :password ].each do |n|
          raise ValidatorError, "'#{n}' is required." unless submission.key?(n)
        end

        ValidatorResult.new(
          successful: @username == submission[:username] && @password_hash == submission[:password],
          environment: {}
        )
      end
    end
  end
end
