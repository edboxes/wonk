module Wonk
  module PolicyValidators
    class ValidatorResult
      attr_reader :environment

      def initialize(successful:, environment:)
        @successful = !!successful
        @environment = environment.deep_dup.deep_symbolize_keys
        raise "environment must be a Hash." unless @environment.is_a?(Hash)

        @environment[:captures] ||= {}
      end

      def success?
        @successful
      end
    end
  end
end
