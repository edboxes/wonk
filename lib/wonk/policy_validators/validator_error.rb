require 'wonk/error'

module Wonk
  module PolicyValidators
    class ValidatorError < Wonk::Error; end
  end
end
