require 'wonk/policy_validators/validator'
require 'wonk/policy_validators/aws_ec2/rule'
require 'wonk/policy_validators/aws_ec2/has_role_with_rule'

require 'aws-sdk'

module Wonk
  module PolicyValidators
    class AwsEC2Validator < Validator
      AWS_PUBLIC_CERTIFICATE = <<-PKCS.strip_heredoc
        -----BEGIN CERTIFICATE-----
        MIIC7TCCAq0CCQCWukjZ5V4aZzAJBgcqhkjOOAQDMFwxCzAJBgNVBAYTAlVTMRkw
        FwYDVQQIExBXYXNoaW5ndG9uIFN0YXRlMRAwDgYDVQQHEwdTZWF0dGxlMSAwHgYD
        VQQKExdBbWF6b24gV2ViIFNlcnZpY2VzIExMQzAeFw0xMjAxMDUxMjU2MTJaFw0z
        ODAxMDUxMjU2MTJaMFwxCzAJBgNVBAYTAlVTMRkwFwYDVQQIExBXYXNoaW5ndG9u
        IFN0YXRlMRAwDgYDVQQHEwdTZWF0dGxlMSAwHgYDVQQKExdBbWF6b24gV2ViIFNl
        cnZpY2VzIExMQzCCAbcwggEsBgcqhkjOOAQBMIIBHwKBgQCjkvcS2bb1VQ4yt/5e
        ih5OO6kK/n1Lzllr7D8ZwtQP8fOEpp5E2ng+D6Ud1Z1gYipr58Kj3nssSNpI6bX3
        VyIQzK7wLclnd/YozqNNmgIyZecN7EglK9ITHJLP+x8FtUpt3QbyYXJdmVMegN6P
        hviYt5JH/nYl4hh3Pa1HJdskgQIVALVJ3ER11+Ko4tP6nwvHwh6+ERYRAoGBAI1j
        k+tkqMVHuAFcvAGKocTgsjJem6/5qomzJuKDmbJNu9Qxw3rAotXau8Qe+MBcJl/U
        hhy1KHVpCGl9fueQ2s6IL0CaO/buycU1CiYQk40KNHCcHfNiZbdlx1E9rpUp7bnF
        lRa2v1ntMX3caRVDdbtPEWmdxSCYsYFDk4mZrOLBA4GEAAKBgEbmeve5f8LIE/Gf
        MNmP9CM5eovQOGx5ho8WqD+aTebs+k2tn92BBPqeZqpWRa5P/+jrdKml1qx4llHW
        MXrs3IgIb6+hUIB+S8dz8/mmO0bpr76RoZVCXYab2CZedFut7qc3WUH9+EUAH5mw
        vSeDCOUMYQR7R9LINYwouHIziqQYMAkGByqGSM44BAMDLwAwLAIUWXBlk40xTwSw
        7HX32MxXYruse9ACFBNGmdX2ZBrVNGrN9N2f6ROk0k9K
        -----END CERTIFICATE-----
      PKCS

      AWS_GOVCLOUD_CERTIFICATE = <<-PKCS.strip_heredoc
        -----BEGIN CERTIFICATE-----
        MIIC7TCCAq0CCQCWukjZ5V4aZzAJBgcqhkjOOAQDMFwxCzAJBgNVBAYTAlVTMRkw
        FwYDVQQIExBXYXNoaW5ndG9uIFN0YXRlMRAwDgYDVQQHEwdTZWF0dGxlMSAwHgYD
        VQQKExdBbWF6b24gV2ViIFNlcnZpY2VzIExMQzAeFw0xMjAxMDUxMjU2MTJaFw0z
        ODAxMDUxMjU2MTJaMFwxCzAJBgNVBAYTAlVTMRkwFwYDVQQIExBXYXNoaW5ndG9u
        IFN0YXRlMRAwDgYDVQQHEwdTZWF0dGxlMSAwHgYDVQQKExdBbWF6b24gV2ViIFNl
        cnZpY2VzIExMQzCCAbcwggEsBgcqhkjOOAQBMIIBHwKBgQCjkvcS2bb1VQ4yt/5e
        ih5OO6kK/n1Lzllr7D8ZwtQP8fOEpp5E2ng+D6Ud1Z1gYipr58Kj3nssSNpI6bX3
        VyIQzK7wLclnd/YozqNNmgIyZecN7EglK9ITHJLP+x8FtUpt3QbyYXJdmVMegN6P
        hviYt5JH/nYl4hh3Pa1HJdskgQIVALVJ3ER11+Ko4tP6nwvHwh6+ERYRAoGBAI1j
        k+tkqMVHuAFcvAGKocTgsjJem6/5qomzJuKDmbJNu9Qxw3rAotXau8Qe+MBcJl/U
        hhy1KHVpCGl9fueQ2s6IL0CaO/buycU1CiYQk40KNHCcHfNiZbdlx1E9rpUp7bnF
        lRa2v1ntMX3caRVDdbtPEWmdxSCYsYFDk4mZrOLBA4GEAAKBgEbmeve5f8LIE/Gf
        MNmP9CM5eovQOGx5ho8WqD+aTebs+k2tn92BBPqeZqpWRa5P/+jrdKml1qx4llHW
        MXrs3IgIb6+hUIB+S8dz8/mmO0bpr76RoZVCXYab2CZedFut7qc3WUH9+EUAH5mw
        vSeDCOUMYQR7R9LINYwouHIziqQYMAkGByqGSM44BAMDLwAwLAIUWXBlk40xTwSw
        7HX32MxXYruse9ACFBNGmdX2ZBrVNGrN9N2f6ROk0k9K
        -----END CERTIFICATE-----
      PKCS

      RULES_MAP = {
        'has-role-with' => Wonk::PolicyValidators::AwsEC2::HasRoleWithRule
      }

      attr_reader :rules

      def initialize(parameters)
        raise "Wonk.aws_region must be set to use AwsEC2Validator." if Wonk.aws_region.nil?

        @identity_cert =
          case Wonk.aws_region
          when 'us-gov-west-1'
            AWS_GOVCLOUD_CERTIFICATE
          else
            AWS_PUBLIC_CERTIFICATE
          end

        @ec2_rsrc = Aws::EC2::Resource.new(region: Wonk.aws_region)
        @iam_rsrc = Aws::IAM::Resource.new(region: Wonk.aws_region)

        @rules =
          (parameters[:rules] || []).map do |rule_definition|
            rule_class = RULES_MAP[rule_definition[:type]]

            raise "no rule class for type '#{rule_definition[:type]}'" if rule_class.nil?

            rule_class.new(rule_definition[:parameters] || {})
          end.freeze
      end

      def validator_name
        'aws-ec2'
      end

      def do_authenticate(submission)
        env = { captures: {} }

        success =
          [ :document, :signature ].each do |n|
            raise ValidatorError, "'#{n}' is required." unless submission.key?(n)
          end

          pemmed_signature = <<-PKCS.strip_heredoc
-----BEGIN PKCS7-----
#{submission[:signature]}
-----END PKCS7-----
          PKCS

          Dir.mktmpdir do |dir|
            cert_path = "#{dir}/cert.pem"
            signature_path = "#{dir}/signature.pem"
            data_path = "#{dir}/data.json"

            IO.write(cert_path, @identity_cert)
            IO.write(signature_path, pemmed_signature)
            IO.write(data_path, submission[:document])

            `openssl smime -verify -inform PEM -in '#{signature_path}' -content '#{data_path}' -certfile '#{cert_path}' -noverify > /dev/null 2>&1`

            if $?.success?
              instance_identity = JSON.parse(submission[:document]).deep_symbolize_keys

              instance_id = instance_identity[:instanceId]

              env[:instance_id] = instance_identity[:instanceId]
              env[:account_id] = instance_identity[:accountId]

              instance = @ec2_rsrc.instance(instance_id)

              rule_result =
                begin
                  @rules.map { |rule| rule.try_match(instance, instance_identity) }.find(&:success?)
                rescue Aws::Errors::MissingCredentialsError => err
                  Wonk.logger.error "No AWS credentials found!"
                  raise err
                end

              if !rule_result.nil?
                env[:captures].merge!(rule_result.captures)

                true
              else
                false
              end
            else
              false
            end
          end

        ValidatorResult.new(successful: success, environment: env)
      end
    end
  end
end
