module Wonk
  class PolicyResult
    attr_reader :concretized_content

    def initialize(successful:, concretized_content: nil)
      @successful = !!successful

      @concretized_content = IceNine.deep_freeze((concretized_content || []).deep_dup)
    end

    def success?
      @successful
    end
  end
end
