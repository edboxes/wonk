require 'wonk/policy_validators/validator'
require 'wonk/policy_validators/aws_ec2_validator'
require 'wonk/policy_validators/username_password_validator'

require 'wonk/policy_result'

module Wonk
  class Policy
    VALIDATORS = {
      'username-password' => Wonk::PolicyValidators::UsernamePasswordValidator,
      'aws-ec2' => Wonk::PolicyValidators::AwsEC2Validator
    }

    attr_reader :validators
    attr_reader :content

    def initialize(validators:, content:)
      raise "all entries in 'validators' must be of type Wonk::PolicyValidator::Validator" \
        unless validators.all? { |v| v.is_a?(Wonk::PolicyValidators::Validator) }
      raise "'content' must be an Array." unless content.is_a?(Array)

      @validators = IceNine.deep_freeze(validators.map { |v| v.clone })
      @content = IceNine.deep_freeze(content.map { |r| r.deep_dup })
    end

    def authenticate_from_submission(submission)
      passed_validators =
        validators.map do |v|
          [ v, v.authenticate_from_submission(submission) ]
        end.select { |vt| vt[1].success? }

      concretizer = self.class.concretizer_class.new

      concretized_content =
        content.map do |c|
          # TODO: these should probably be made into a separate class, but marshaling is annoying
          passed_validators.map do |vt|
            validator = vt[0]
            validator_result = vt[1]

            concretize_recursively(c, concretizer, validator_result.environment)
          end
        end

      PolicyResult.new(successful: !passed_validators.empty?,
                       concretized_content: concretized_content)
    end

    private

    def concretize_recursively(value, concretizer, environment)
      if value.is_a?(String)
        concretizer.concretize(value, environment).join('')
      elsif value.respond_to?(:each_pair) # is a hash or hashlike object
        value.map { |k, v| [ k, concretize_recursively(v, concretizer, environment) ] }.to_h
      elsif value.respond_to?(:map)
        value.map { |v| concretize_recursively(v, concretizer, environment) }
      else
        value
      end
    end

    def self.concretizer_class
      Concretizer
    end


    def self.from_hash(name, hash)
      hash = hash.deep_symbolize_keys
      validator_hashes = hash[:validators] || []
      content = hash[:content] || []

      raise "policy '#{name}' must have at least one validator." \
        unless validator_hashes.is_a?(Array) && !validator_hashes.empty?
      raise "policy '#{name}' must have a 'content' array, even if it's empty." \
        unless content.is_a?(Array)


      validators =
        validator_hashes.map do |vh|
          vc = VALIDATORS[vh[:type]]

          raise "policy '#{name}' has no validator for type '#{vh[:type]}'." if vc.nil?

          vc.new(vh[:parameters] || {})
        end

      Policy.new(validators: validators, content: content)
    end
  end
end
